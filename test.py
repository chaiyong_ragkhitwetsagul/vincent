"""
Copyright 2017 Chaiyong Ragkhitwetsagul

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import math
import numpy as np
from scipy.misc import imread
from scipy import sum, average
import cv2 as cv
from vincent import *

# def get_dist_arr(size1, size2):
#     """
#     Generate Euclidean ground distance array for EMD
#     :param size1: size of the first vector
#     :param size2: size of the second vector
#     :return: the ground distance array
#     """
#     dist_arr = np.zeros((size1 * size1, size2 * size2), dtype=float)
#     # count = 1
#     for ar in range(0, size1):
#         for ac in range(0, size1):
#             for br in range(0, size2):
#                 for bc in range(0, size2):
#                     dist = math.sqrt((ar - br) * (ar - br) + (ac - bc) * (ac - bc))
#                     dist_arr[ar * size1 + ac, br * size2 + bc] = dist
#                     # print(count, ar * size1 + ac, br * size2 + bc)
#                     # count += 1
#
#     return dist_arr
#
#
# print(get_dist_arr(6, 6))

# def to_grayscale(arr):
#     # If arr is a color image (3D array), convert it to grayscale (2D array).
#     if len(arr.shape) == 3:
#         return average(arr, -1)  # average over the last axis (color channels)
#     else:
#         return arr
#
#
# # data = to_grayscale(imread('output/0.png').astype(float))
# data1 = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])
# data2 = np.array([[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]])
# print(data1)
# print(data1[0:2, 2:4])
# histEMD(data1, data2)
# print(sum(data))
# mult = 2
# s1 = int(data.shape[0]/mult)
# s2 = int(data.shape[1]/mult)
# first = np.zeros(s1 * s2, dtype=float)
# count = 0
# for i in range(s1):
#     for j in range(s2):
#         first[count] = sum(data[i * mult: i * mult + mult, j * mult: j * mult + mult])
#         count += 1
#
# print(first)

def to_grayscale(arr):
    # If arr is a color image (3D array), convert it to grayscale (2D array).
    if len(arr.shape) == 3:
        return average(arr, -1)  # average over the last axis (color channels)
    else:
        return arr


def blur_image(arr):
    # copied from
    # https://stackoverflow.com/questions/10965417/
    im = Image.fromarray(arr)
    # copied from https://stackoverflow.com/questions/16720682/pil-cannot-write-mode-f-to-jpeg
    if im.mode != 'RGB':
        im = im.convert('RGB')
    # adapted from https://stackoverflow.com/questions/21215903/blurring-an-image-using-pil-in-python
    # and http://pillow.readthedocs.io/en/latest/reference/ImageFilter.html
    # im = im.filter(ImageFilter.BLUR)
    im = im.filter(ImageFilter.GaussianBlur(radius=1))
    # im = im.filter(ImageFilter.BoxBlur(radius=1))
    im.save('img1_cnv_b.png')
    return np.array(im)


data = imread('images/img1_cnv.png')
# make sure the image does not exceed the predefined size
data = data[0: 1000, 0: 1000]
img = to_grayscale(data.astype(float))
blur_image(img)