#!/usr/bin/env python

"""
Copyright 2017 Chaiyong Ragkhitwetsagul

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# from scipy.misc import imsave
from imageio import imread
from imageio import imwrite
from scipy import average
import numpy as np
from PIL import Image, ImageFilter
from converter import *
import glob2
import shutil
import datetime
import time
from pyemd import emd
import pandas
from multiprocessing.dummy import Pool as ThreadPool
from lxml import etree as et
import argparse
import os
import math


# global vars because we're working with threads here
image_files = []
image_info = []
mappings = []
threshold = 0.25
min_size = 10
max_size = 100
threads = 10
binsize = 50
img_dim = 300
sim_measure = 'jaccard'
blur = False
blur_radius = 20
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')
input_dir = ''
output_dir = ''

gcf_root = et.Element("CloneClasses")


def main():
    global input_dir, output_dir, threshold, min_size, max_size, sim_measure, blur, blur_radius
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='input folder', required=True)
    parser.add_argument('-o', '--output', help='output folder', required=True)
    parser.add_argument('-t', '--threshold', help='similarity threshold', required=False)
    parser.add_argument('-mn', '--minsize', help='min clone size', required=False)
    parser.add_argument('-mx', '--maxsize', help='max clone size', required=False)
    parser.add_argument('-s', '--sim', help='similarity measure [jaccard, emd]', required=False)
    parser.add_argument('-b', '--blur', help='blurring images', required=False, action='store_true')
    parser.add_argument('-r', '--radius', help='Gaussian blur radius', required=False)
    args = parser.parse_args()

    input_dir = args.input
    output_dir = args.output
    if not input_dir.endswith('/'):
        input_dir = input_dir + '/'
    if not output_dir.endswith('/'):
        output_dir = output_dir + '/'

    if args.threshold is not None:
        threshold = float(args.threshold)
    if args.minsize is not None:
        min_size = int(args.minsize)
    if args.maxsize is not None:
        max_size = int(args.maxsize)
    if args.sim is not None:
        sim_measure = args.sim
    if args.blur:
        blur = True
    if args.radius:
        blur_radius = int(args.radius)

    print('=' * 30)
    print('Running Vincent with config below')
    print('Input          : ' + input_dir)
    print('Output         : ' + output_dir)
    print('Similarity     : ' + sim_measure)
    print('Threshold      :', threshold)
    print('Min clone size :', min_size)
    print('Max clone size :', max_size)
    print('Blur           :', blur)
    print('=' * 30)

    # remove output folder if it exist
    if os.path.exists(output_dir):
        # print("deleting " + output_dir)
        # try:
        #     shutil.rmtree(output_dir)
        # except Exception as e:
        #     print(e)
        print('output directory exists. please delete the directory or choose a new output location')
        exit(0)
    else:
        os.makedirs(output_dir)
    # copy the css to the output folder
    shutil.copyfile('highlight_w.css', output_dir + 'highlight_w.css')
    extract_method(input_dir, output_dir, min_size, max_size)

    # read method-id mapping
    global mappings
    mappings = pandas.read_csv('mapping.csv')
    # print(mappings.loc[0]['file'])

    methods = get_methods(output_dir, '*.java')
    print('Found: ', len(methods), 'method')

    # FIRST PHASE: convert code files to PNG images
    # run in threads to speed things up
    # copied from
    # https://stackoverflow.com/questions/2846653/how-to-use-threading-in-python
    # make the Pool of workers
    pool = ThreadPool(threads)
    pool.map(java_to_img, methods)
    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    images = get_methods(output_dir, '*.png')
    print('Found:', len(images), 'images')

    # SECOND PHASE: read all images into memory
    global image_files
    global image_info
    for idx, imgfile in enumerate(images):
        # read images as 2D arrays (convert to grayscale for simplicity)
        data = imread(imgfile)
        # make sure the image does not exceed the predefined size
        data = data[0: img_dim, 0: img_dim]
        image_files.append(to_grayscale(data.astype(float)))
        image_info.append(imgfile)

    # THIRD PHASE: similarity detection
    pairs = []
    for idx, img1 in enumerate(image_files):
        for idx2, img2 in enumerate(image_files[idx + 1:]):
            pair = [idx, img1, idx2, img2]
            pairs.append(pair)

    print('-' * 50)

    # ## multi-thread again for the pair-wise similarity comparisons
    pool = ThreadPool(threads)

    if sim_measure == 'jaccard':
        pool.map(compare, pairs)
    elif sim_measure == 'emd':
        pool.map(compare_emd, pairs)
    else:
        print('ERROR: the similarity measure ' + sim_measure + ' is not available.')
        exit()
    pool.close()
    pool.join()

    tree = et.ElementTree(gcf_root)
    tree.write(output_dir + "clones.gcf", pretty_print=True)

    # clean_up(output)


def compare(pair):
    if not blur:
        sim_orig = compare_image_pair(pair)
    else:
        sim_orig = compare_image_pair_blur(pair)
    # report clones if similarity is higher than the threshold
    if sim_orig >= threshold:
        id1 = int(image_info[pair[0]].replace(".png", "").replace(output_dir, ""))
        id2 = int(image_info[pair[0] + pair[2] + 1].replace(".png", "").replace(output_dir, ""))
        # print(id1, id2)
        print(str(sim_orig)
              + "," + mappings.loc[id1]['file'] + "," + str(mappings.loc[id1]['start']) + "," + str(
            mappings.loc[id1]['end'])
              + "," + mappings.loc[id2]['file'] + "," + str(mappings.loc[id2]['start']) + "," + str(
            mappings.loc[id2]['end']))

        writefile(output_dir + "clones.csv",
                  str(sim_orig)
                  # + "," + image_info[idx].replace(".png", "").replace(output, "")
                  + "," + mappings.loc[id1]['file'] + "," + str(mappings.loc[id1]['start']) + "," + str(
                      mappings.loc[id1]['end'])
                  + "," + mappings.loc[id2]['file'] + "," + str(mappings.loc[id2]['start']) + "," + str(
                      mappings.loc[id2]['end'])
                  + "\n", 'a', False)

        clone_class = et.SubElement(gcf_root, "CloneClass")
        et.SubElement(clone_class, "ID").text = str(pair[0])
        clone1 = et.SubElement(clone_class, "Clone")
        fragment1 = et.SubElement(clone1, "Fragment")
        et.SubElement(fragment1, "File").text = mappings.loc[id1]['file']
        et.SubElement(fragment1, "Start").text = str(mappings.loc[id1]['start'])
        et.SubElement(fragment1, "End").text = str(mappings.loc[id1]['end'])
        clone2 = et.SubElement(clone_class, "Clone")
        fragment2 = et.SubElement(clone2, "Fragment")
        et.SubElement(fragment2, "File").text = mappings.loc[id2]['file']
        et.SubElement(fragment2, "Start").text = str(mappings.loc[id2]['start'])
        et.SubElement(fragment2, "End").text = str(mappings.loc[id2]['end'])


def compare_emd(pair):
    id1 = int(image_info[pair[0]].replace(".png", "").replace(output_dir, ""))
    id2 = int(image_info[pair[0] + pair[2] + 1].replace(".png", "").replace(output_dir, ""))
    img1 = pair[1]
    img2 = pair[3]

    s1 = int(img1.shape[0] / binsize)
    s2 = int(img1.shape[1] / binsize)
    first = np.zeros(s1 * s2, dtype=float)
    second = np.zeros(s1 * s2, dtype=float)

    count = 0
    for i in range(s1):
        for j in range(s2):
            img1_cnv = abs(img1 - 255)
            sub = img1_cnv[i * binsize: i * binsize + binsize, j * binsize: j * binsize + binsize]
            # first[count] = np.count_nonzero(sub)
            first[count] = np.sum(sub)
            count += 1

    count = 0
    for i in range(s1):
        for j in range(s2):
            img2_cnv = abs(img2 - 255)
            sub = img2_cnv[i * binsize: i * binsize + binsize, j * binsize: j * binsize + binsize]
            second[count] = np.sum(sub)
            count += 1

    distance_matrix = get_dist_arr(s1, s2)
    dist, norm_dist = compare_images_emd(first, second, distance_matrix)

    sim = 1 - norm_dist
    if sim > threshold:
        print(sim, end=',')
        print(mappings.loc[id1]['file'] + "," + str(mappings.loc[id1]['start']) + "," + str(
            mappings.loc[id1]['end'])
              + "," + mappings.loc[id2]['file'] + "," + str(mappings.loc[id2]['start']) + "," + str(
            mappings.loc[id2]['end']))

        writefile(output_dir + "clones.csv",
                  str(sim)
                  # + "," + image_info[idx].replace(".png", "").replace(output, "")
                  + "," + mappings.loc[id1]['file'] + "," + str(mappings.loc[id1]['start']) + "," + str(
                      mappings.loc[id1]['end'])
                  + "," + mappings.loc[id2]['file'] + "," + str(mappings.loc[id2]['start']) + "," + str(
                      mappings.loc[id2]['end'])
                  + "\n", 'a', False)

        clone_class = et.SubElement(gcf_root, "CloneClass")
        et.SubElement(clone_class, "ID").text = str(pair[0])
        clone1 = et.SubElement(clone_class, "Clone")
        fragment1 = et.SubElement(clone1, "Fragment")
        et.SubElement(fragment1, "File").text = mappings.loc[id1]['file']
        et.SubElement(fragment1, "Start").text = str(mappings.loc[id1]['start'])
        et.SubElement(fragment1, "End").text = str(mappings.loc[id1]['end'])
        clone2 = et.SubElement(clone_class, "Clone")
        fragment2 = et.SubElement(clone2, "Fragment")
        et.SubElement(fragment2, "File").text = mappings.loc[id2]['file']
        et.SubElement(fragment2, "Start").text = str(mappings.loc[id2]['start'])
        et.SubElement(fragment2, "End").text = str(mappings.loc[id2]['end'])


def java_to_img(m):
    java_to_html(m)
    html1 = m.replace(".java", "") + ".html"
    html_to_image(html1)


def compare_image_pair_blur(pair):
    # normalize to compensate for exposure difference
    img1 = normalize(pair[1])
    img2 = normalize(pair[3])
    # converting and blurring images
    img1_cnv = abs(img1 - 255)
    img1_cnvb = blur_image(img1_cnv)
    img2_cnv = abs(img2 - 255)
    img2_cnvb = blur_image(img2_cnv)

    imwrite('1b.png', img1_cnvb)
    imwrite('2b.png', img2_cnvb)

    # calculate the difference and its norms
    #  element-wise for scipy arrays
    diff_cnv = img1_cnvb - img2_cnvb
    nzero_img = np.count_nonzero(img1_cnvb + img2_cnvb)
    nzero_diff = np.count_nonzero(diff_cnv)
    sim = 1 - nzero_diff / nzero_img

    return sim


def compare_images_emd(img1, img2, distance_matrix):
    """
    Computer the Earth Mover's Distance of two images
    :param img1: the first image
    :param img2: the second image
    :return: distance, normalised distance [0, 1]
    """
    # adapted from
    # https://stackoverflow.com/questions/3337301/numpy-matrix-to-array
    first = img1.flatten()
    second = img2.flatten()
    dist = emd(first, second, distance_matrix)

    sum = first + second
    blank_parts = np.where(sum == 0)[0]
    first = np.delete(first, blank_parts)
    second = np.delete(second, blank_parts)

    black = np.zeros(first.size, dtype=float)
    white = np.full(second.size, 255 * binsize * binsize, dtype=float)
    max_dist = emd(black, white, distance_matrix)

    return dist, dist/max_dist


def compare_images(img1, img2):
    # some part of the image comparison implementation is adapted from
    # https://stackoverflow.com/questions/189943/how-can-i-quantify-difference-between-two-images
    # normalize to compensate for exposure difference
    img1 = normalize(img1)
    img2 = normalize(img2)
    # calculate the difference and its norms
    img1_cnv = abs(img1 - 255)
    img2_cnv = abs(img2 - 255)
    diff_cnv = img1_cnv - img2_cnv
    nzero_img = np.count_nonzero(img1_cnv + img2_cnv)
    nzero_diff = np.count_nonzero(diff_cnv)
    simorig = 1 - nzero_diff / nzero_img

    return simorig


def compare_image_pair(pair):
    # normalize to compensate for exposure difference
    img1 = normalize(pair[1])
    img2 = normalize(pair[3])
    # calculate the difference and its norms
    # diff = (img1 - img2)/255
    #  element-wise for scipy arrays
    img1_cnv = abs(img1 - 255)
    img2_cnv = abs(img2 - 255)
    diff_cnv = img1_cnv - img2_cnv

    nzero_img = np.count_nonzero(img1_cnv + img2_cnv)
    nzero_diff = np.count_nonzero(diff_cnv)

    simorig = 1 - nzero_diff / nzero_img

    return simorig


def blur_image(arr):
    # copied from
    # https://stackoverflow.com/questions/10965417/
    im = Image.fromarray(arr)
    # copied from https://stackoverflow.com/questions/16720682/pil-cannot-write-mode-f-to-jpeg
    if im.mode != 'RGB':
        im = im.convert('RGB')
    # adapted from https://stackoverflow.com/questions/21215903/blurring-an-image-using-pil-in-python
    # and http://pillow.readthedocs.io/en/latest/reference/ImageFilter.html
    # im = im.filter(ImageFilter.BLUR)
    im = im.filter(ImageFilter.GaussianBlur(radius=blur_radius))
    # im = im.filter(ImageFilter.BoxBlur(radius=1))
    im.save('img1_cnv_b.png')
    return np.array(im)


def to_grayscale(arr):
    # If arr is a color image (3D array), convert it to grayscale (2D array).
    if len(arr.shape) == 3:
        return average(arr, -1)  # average over the last axis (color channels)
    else:
        return arr


def normalize(arr):
    rng = arr.max() - arr.min()
    amin = arr.min()
    if rng != 0:
        return (arr-amin) * 255 / rng
    else:
        return arr


def print_matrix(a):
    print("Matrix[" + ("%d" % a.shape[0]) + "][" + ("%d" % a.shape[1]) + "]")
    rows = a.shape[0]
    cols = a.shape[1]
    for i in range(0, rows):
        for j in range(0, cols):
            if a[i, j] != 0:
                print("%6.f" % a[i, j], end='')
        print()
    print()


def get_methods(location, extension):
    """
    Get files in a folder (location) recursively
    :param location: the location to search for files
    :param extension: the file extension to search for
    :return: list of all matched files
    """
    all_files = glob2.glob(location + '/**/' + extension)
    return all_files


def writefile(filename, fcontent, mode, is_print):
    """
    Write the string content to a file
    copied from
    http://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
    :param filename: name of the file
    :param fcontent: string content to put into the file
    :param mode: writing mode, 'w' overwrite every time, 'a' append to an existing file
    :return: N/A
    """
    # try:
    file = open(filename, mode)
    file.write(fcontent)
    file.close()

    if is_print:
        print("saved:" + filename)
    # except Exception as e:
    #     print '-' * 60
    #     traceback.print_exc(file=sys.stdout)
    #     print '-' * 60


def get_dist_arr(size1, size2):
    """
    Generate Euclidean ground distance array for EMD
    :param size1: size of the first vector
    :param size2: size of the second vector
    :return: the ground distance array
    """
    dist_arr = np.zeros((size1 * size1, size2 * size2), dtype=float)
    # count = 1
    for ar in range(0, size1):
        for ac in range(0, size1):
            for br in range(0, size2):
                for bc in range(0, size2):
                    dist = math.sqrt((ar - br) * (ar - br) + (ac - bc) * (ac - bc))
                    dist_arr[ar * size1 + ac, br * size2 + bc] = dist
                    # print(count, ar * size1 + ac, br * size2 + bc)
                    # count += 1

    return dist_arr


def clean_up(dir):
    """
    Delete all files with .html, .java, .png from the folder
    :param dir: a given folder
    :return: None
    """
    files = os.listdir(dir)
    for file in files:
        if file.endswith(".html") or file.endswith(".java") or file.endswith(".png") or file.endswith(".css"):
            os.remove(os.path.join(dir, file))


if __name__ == "__main__":
    main()
