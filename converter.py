"""
Copyright 2017 Chaiyong Ragkhitwetsagul

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import imgkit
import sys
from subprocess import Popen, PIPE


def html_to_image(input):
    options = {
        'format': 'png',
        'encoding': "UTF-8",
        'quality': 100,
        'width': 300,
        'height': 300,
        'zoom': 1
    }

    print(input)
    output = input.replace(".html", ".png")
    # img = imgkit.from_file(input, False)
    imgkit.from_file(input, output, options=options)
    # return img, output


def java_to_html(input):
    p = Popen(["./tools/format.sh " + input], stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    output, err = p.communicate()
    # print('Java->HTML ' + str(output).strip())


def extract_method(dir, outputdir, min_size, max_size):
    print("input: " + dir)
    print("output: " + outputdir)
    p = Popen(["java -jar ./tools/MethodParser-1.5.jar " + dir + " " + outputdir + " " + str(min_size) + " " + str(max_size)], stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    output, err = p.communicate()
    # print(output.strip())
    # print(err.strip())
