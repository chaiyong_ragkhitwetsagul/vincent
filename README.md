# Vincent #

An image-based clone detection tool

### How do I get set up? ###

Vincent is a combination of multiple tools and libraries. 
To sucessfully execute Vincent, you need the following dependencies installed on your machine.
Vicent has been successfully executed on macOS and Ubuntu 18.04.5 LTS (bionic).

1. Java (tested with Java 7 & 8) for executing a Java method parser
2. Python (preferably with [conda](https://conda.io/miniconda.html), tested with Python 3.6) to execute other parts of Vincent
3. [wkhtmltopdf](https://wkhtmltopdf.org) to convert HTML to an image. Version [0.12.5](https://github.com/wkhtmltopdf/wkhtmltopdf/releases/0.12.5/) is tested to work.
4. Several python supporting libraries.

#### Configuration steps ####
The following steps show how to install Vincent on an Ubuntu and a macOS machine.

Install Java
```bash
sudo apt-get install default-jdk
```

Clone Vincent

```bash
cd <YOUR VINCENT_HOME>
git clone git@bitbucket.org:chaiyong_ragkhitwetsagul/vincent.git
```

Compile the `uncomment` program.
```bash
cd vincent
cd tools
gcc -o uncomment-java uncomment-java.c
```

Install miniconda with Python 3.6 (if you're using a mac, 
download [miniconda for macOS](https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh) instead).

```bash
cd ~
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod 755 Miniconda3-latest-Linux-x86_64.sh 
./Miniconda3-latest-Linux-x86_64.sh 
```
After installing conda, you need to open a new terminal to be able to execute conda command.
Then, create a new virtual environment. We are going to install all the Python dependencies of
Vincent here.

```bash
conda create --name vincent
source activate vincent
conda install scipy pyemd
pip install image glob2 pandas lxml
```

Install wkhtmltopdf (note that Vincent is tested with only wkhtmltopdf version 0.12.5):

Ubuntu `sudo apt-get install wkhtmltopdf`
macOS `brew install wkhtmltopdf`

And then install imgkit:

```bash
pip install imgkit
```

You'll be ready to go! Try executing Vincent by:

```bash
cd <YOUR VINCENT_HOME>/vincent
python vincent.py
```

And you should see the usage printed out like below:
```
usage: vincent.py [-h] -i INPUT -o OUTPUT [-t THRESHOLD] [-mn MINSIZE]
                  [-mx MAXSIZE] [-s SIM] [-b] [-r RADIUS]
vincent.py: error: the following arguments are required: -i/--input, -o/--output
```

### How to run Vincent ###

Vincent accepts the following parameters:

* -i (INPUT): A full path to the source code to detect clones
* -o (OUTPUT): A full path to the output folder where Vincent keeps all the working files (images, htmls, etc.)
* -t (THRESHOLD): A clone similarity cut-off threshold (default 0.25 for Jaccard, and 0.9985 for EMD)
* -mn (MINSIZE): The minimum method size to be analysed
* -mx (MAXSIZE): The maximum method size to be analysed
* -s (SIM): The selected similarity computation (either jaccard or emd)
* -b : Add Gaussian blur
* -r : Set the radius of the Gaussian blur (only works if -b is given)

Example:
```bash
# Jaccard similarity
python vincent.py -i /my/software/project -o output -t 0.25 -mn 10 -mx 100 -s jaccard
# Add blurring
python vincent.py -i /my/software/project -o output -t 0.25 -mn 10 -mx 100 -s jaccard -b -r 3
# EMD similarity
python vincent.py -i /my/software/project -o output -t 0.9985 -mn 10 -mx 100 -s emd
```

The detected clone pairs will be reported in a file called clones.csv (CSV format) and clones.gcf (General Clone Format -- see Wang et al., *Searching for Better Configurations: A Rigorous Approach to Clone Evaluation*, in FSE 2013, pp. 455--465.) 
residing in the output folder.

### Contacts ###

* Chaiyong Ragkhitwetsagul (<chaiyong.rag@mahidol.edu>)