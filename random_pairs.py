import sys
import csv
import random
from vincent import writefile

all_vincent_clones = []
with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        all_vincent_clones.append(row)

max = 50
if len(all_vincent_clones) < max:
    max = len(all_vincent_clones) - 1

rand_clones_str = ''
vim_str = ''
for i in range(max):
    r = random.randrange(1, len(all_vincent_clones), 1)
    row = all_vincent_clones.pop(r)
    print(row)
    rand_clones_str += row[0] + ',' + row[1] + ',' + row[2] + ',' + row[3] + ',' + row[4] + ',' + row[5] + '\n'
    vim_str += 'vim -O ' + row[0] + ' +' + row[1] + ' ' + row[3] + '\n'

writefile(sys.argv[1].replace('.csv', '_rand.csv'), rand_clones_str, 'w', False)
writefile(sys.argv[1].replace('.csv', '_rand.sh'), vim_str, 'w', False)