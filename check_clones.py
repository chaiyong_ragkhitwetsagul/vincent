import xml.etree.ElementTree
import sys
import os
import csv


def writefile(filename, fcontent, mode, isprint):
    """
    Write the string content to a file
    copied from
    http://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
    :param filename: name of the file
    :param fcontent: string content to put into the file
    :param mode: writing mode, 'w' overwrite every time, 'a' append to an existing file
    :return: N/A
    """
    # try:
    file = open(filename, mode)
    file.write(fcontent)
    file.close()


def main():
    old_vincent_clones = dict()
    with open(sys.argv[1], 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            # print(row)
            new_row = row[3] + ',' + row[4] + ',' + row[5] + ',' + row[6] + ',' + row[7] + ',' + row[8]
            # print(new_row)
            old_vincent_clones[new_row] = row

    new_vincent_clones = list()
    with open(sys.argv[2], 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            # print(row)
            new_row = row[0] + ',' + row[1] + ',' + row[2] + ',' + row[3] + ',' + row[4] + ',' + row[5]
            # print(new_row)
            new_vincent_clones.append(new_row)

    for idx, clone in enumerate(new_vincent_clones):
        if clone in old_vincent_clones:
            row = old_vincent_clones[clone]
            out = ''
            for r in row:
                out += r + ','
            writefile('disjoint_new.csv', out + '\n', 'a', True)


if __name__ == '__main__':
    main()