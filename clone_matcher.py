import xml.etree.ElementTree
import sys
import os
import csv


def writefile(filename, fcontent, mode, isprint):
    """
    Write the string content to a file
    copied from
    http://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
    :param filename: name of the file
    :param fcontent: string content to put into the file
    :param mode: writing mode, 'w' overwrite every time, 'a' append to an existing file
    :return: N/A
    """
    # try:
    file = open(filename, mode)
    file.write(fcontent)
    file.close()


def main():

    try:
        os.remove('common.csv')
        os.remove('disjoint-n.csv')
        os.remove('disjoint-v.csv')
    except OSError:
        # print("couldn't remove files")
        None

    writefile('common.csv', 'nicad1,start,end,nicad2,start,end,vincent1,start,end,vincent2,start,end\n', 'a', False)
    writefile('disjoint-n.csv', 'nicad1,start,end,nicad2,start,end,vincent1,start,end,vincent2,start,end\n', 'a', False)
    writefile('disjoint-v.csv', 'nicad1,start,end,nicad2,start,end,vincent1,start,end,vincent2,start,end\n', 'a', False)

    all_nicad_clones = []
    nicad = xml.etree.ElementTree.parse(sys.argv[1]).getroot()
    clones = nicad.findall('clone')
    for idx, clone in enumerate(clones):
        sources = clone.findall('source')
        file1 = sources[0].get('file')
        start1 = sources[0].get('startline')
        end1 = sources[0].get('endline')
        file2 = sources[1].get('file')
        start2 = sources[1].get('startline')
        end2 = sources[1].get('endline')
        all_nicad_clones.append([[file1, int(start1), int(end1)], [file2, int(start2), int(end2)]])

    all_vincent_clones = []
    with open(sys.argv[2], 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            # print(row)
            all_vincent_clones.append([[row[1], int(row[2]), int(row[3])], [row[4], int(row[5]), int(row[6])]])

    common_clones1 = []
    common_clones2 = []
    for idx1, nc in enumerate(all_nicad_clones):
        for idx2, vc in enumerate(all_vincent_clones):
            nc1 = nc[0]
            nc2 = nc[1]
            vc1 = vc[0]
            vc2 = vc[1]

            ostr = nc1[0] + ',' + str(nc1[1]) + ',' + str(nc1[2]) + ',' + \
                   nc2[0] + ',' + str(nc2[1]) + ',' + str(nc2[2]) + ',' + \
                   vc1[0] + ',' + str(vc1[1]) + ',' + str(vc1[2]) + ',' + \
                   vc2[0] + ',' + str(vc2[1]) + ',' + str(vc2[2]) + '\n'

            if nc1[0].strip() == vc1[0].strip() \
                    and nc2[0].strip() == vc2[0].strip() \
                    and (abs(nc1[1] - vc1[1]) <= 2 and abs(nc1[2] - vc1[2]) <= 2) \
                    and (abs(nc2[1] - vc2[1]) <= 2 and abs(nc2[2] - vc2[2]) <= 2):
                    writefile('common.csv', ostr, 'a', False)
                    common_clones1.append(idx1)
                    common_clones2.append(idx2)

            elif nc1[0].strip() == vc2[0].strip() \
                    and nc2[0].strip() == vc1[0].strip() \
                    and (abs(nc1[1] - vc2[1]) <= 2 and abs(nc1[2] - vc2[2]) <= 2) \
                    and (abs(nc2[1] - vc1[1]) <= 2 and abs(nc2[2] - vc1[2]) <= 2):
                    writefile('common.csv', ostr, 'a', False)
                    common_clones1.append(idx1)
                    common_clones2.append(idx2)

    # print(common_clones1, common_clones2)
    for idx1, nc in enumerate(all_nicad_clones):
        if idx1 not in common_clones1:
            nc1 = nc[0]
            nc2 = nc[1]
            ostr = nc1[0] + ',' + str(nc1[1]) + ',' + str(nc1[2]) + ',' + \
                   nc2[0] + ',' + str(nc2[1]) + ',' + str(nc2[2]) + ',' + '\n'
            writefile('disjoint-n.csv', ostr, 'a', False)
    for idx2, vc in enumerate(all_vincent_clones):
        if idx2 not in common_clones1:
            vc1 = vc[0]
            vc2 = vc[1]
            ostr = vc1[0] + ',' + str(vc1[1]) + ',' + str(vc1[2]) + ',' + \
                   vc2[0] + ',' + str(vc2[1]) + ',' + str(vc2[2]) + '\n'
            writefile('disjoint-v.csv', ostr, 'a', False)


if __name__ == '__main__':
    main()