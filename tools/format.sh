#!/usr/bin/env bash
name=`echo $1 | sed -e s:.java::`
newname="$name"T.java
echo $name $newname

# remove comments
./tools/uncomment-java < $1 | sed -e '/^[ \t\r]*$/d' > $newname
# pretty printing
astyle --style=java -p -P -d -H -U --delete-empty-lines -j --max-code-length=50 $newname
# convert to html
highlight $newname -O html | sed -e s:highlight.css:highlight_w.css: > $name.html
# clean up
rm "$name"T.java.orig $newname